import styled from 'styled-components';

import Card from './Card'
import Submenu from './Submenu'

const Container = styled.div`
  margin: auto;
`;

function App() {
  return (
    <Container>
      <Card />
      <Submenu />
    </Container>
  );
}

export default App;
