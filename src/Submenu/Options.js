import React, { useState, useEffect } from "react";
import styled from "styled-components";

import check from '../images/check.svg';

const Container = styled.div`
  margin-left: 25px;
  margin-top: 15px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin-right: 25px;
  align-items: center;
`;

const Title = styled.p`
  margin: 0;
  color: white;
  font-weight: 500;
  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
`;

export const Circle = styled.div`
  border-radius: 50%;
  border: 1.8px solid white;
  height: 35px;
  width: 35px;
  cursor: pointer;
  background-color: ${props => props.visible ? 'white' : 'transparent'};
`

export const Icon = styled.img`
  height: 35px;
  width: 35px;
  object-fit: contain;
  visibility: ${props => props.visible ? 'visible' : 'hidden'};
`

const App = ({ title, state }) => {

  const [visible, setVisible] = useState(false)

  useEffect(() => {
    setVisible(state)    
  }, [state])

  const handleChange = () => {
    setVisible(!visible)
  }
  
  return(
    <Container>
      <Title>
        {title}
      </Title>
      <Circle visible={visible} onClick={handleChange}>
        <Icon src={check} visible={visible} />
      </Circle>

    </Container>
  );
};

export default App;