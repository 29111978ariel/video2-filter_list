import React from "react";
import styled from "styled-components";

import Options, { Circle, Icon} from './Options'

import check from '../images/check.svg'
import cancel from '../images/cancel.svg'

const Container = styled.div`
  background: #21d0d0;
  border-radius: 25px;
  padding-top: 15px;
  height: 200px;
  width: 400px;

  /* opacity: 0.8; */
`;

const Footer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  gap: 16px;
`

const App = () => {

  const options = [
    {title:'PRICO LOW TO HIGH', state: false},
    {title:'PRICO HIGH TO LOW', state: false},
    {title:'POPULARITY', state: true},
  ]
  return(
    <Container>
      { options.map((v, i) => <Options key={i} {...v} />) }
      <Footer>
        {[check, cancel].map( (v, i) => {
          return(
            <Circle 
              visible={true}
              style={{height: 48, width: 48}}
            >
              <Icon 
                src={v} 
                visible={true}
                style={{height: 48, width: 48}} 
              />
            </Circle>  
          )
        })}
      </Footer>
    </Container>
  );
};

export default App;